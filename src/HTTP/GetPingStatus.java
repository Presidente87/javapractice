package HTTP;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by leonelzimi on 23/07/2019.
 * HTTP pinger
 */
public class GetPingStatus {

    public static void main(String args[]) throws Exception{

        String[] hostList = {"http://bbc.com",
                "https://amps.dev.monese.com/monese-account-manager-0.1/api/mock/add-direct-debitgit st",
                "http://techcrunch.com/", "https://ebay.co.uk/", "http://google.co.uk/" };

        for (int i = 0; i < hostList.length; i++) {

            String url = hostList[i];
            getStatus(url);

        }

        System.out.println("Task completed...");
    }

    public static String getStatus(String url) throws IOException {

        String result = "";
        int code = 200;
        try {
            URL siteURL = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(3000);
            connection.connect();

            code = connection.getResponseCode();
            if (code == 200) {
                result = "-> Green <-\t" + "Code: " + code;
                ;
            } else {
                result = "-> Yellow <-\t" + "Code: " + code;
            }
        } catch (Exception e) {
            result = "-> Red <-\t" + "Wrong domain - Exception: " + e.getMessage();

        }
        System.out.println(url + "\t\tStatus:" + result);
        return result;
    }

    }

