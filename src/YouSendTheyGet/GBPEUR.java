package YouSendTheyGet;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 * Created by leonelzimi on 17/07/2019.
 *
 * TO do add currency pairs and allow user to select a currency from the list to determine fee
 * Add comparison logic to ensure the correct minimum fee is applied
 * Retrieve rate and minimum amount from get-all-currencies-v2 and compare rate
 * with the rate in the currency_buffer table
 * Add method to retrieve pricing plan via Monese reference
 *
 */
public class GBPEUR {

        public static void main(String arg[]) {

            double fee;

            Scanner scanner = new Scanner(System.in);

            String planName;

            System.out.println("Please enter confirm which pricing plan you are on. 1 = Simple, " +
                    "2 = Classic, 3 = Premium, 4 = Starter, 5 = Original and 6 = Business");

            int pricingPlan = scanner.nextInt();

            switch (pricingPlan)
            {
                case 1: planName = "Simple";
                break;

                case 2: planName = "Classic";
                break;

                case 3: planName = "Premium";
                break;

                case 4: planName = "Starter";
                break;

                case 5: planName = "Original";
                break;

                case 6: planName = "Business";
                break;

                default: planName = "Invalid pricing plan option selected";
                break;
            }



            System.out.println("The pricing plan that you have selected is " + planName);

            if (planName.equals("Simple") || planName.equals("Classic") || planName.equals("Premium") || planName.equals("Original") || planName.equals("Business"))
            {
                fee = 0;
            }

            else if (planName.equals("Starter"))
            {

                fee = 1;
            }

            else {
                fee = 0;
            }

            System.out.println("Enter the minimum transfer amount");
            double minTransfer = scanner.nextDouble();

            System.out.println("Enter the amount you would like to send");
            double youSend = scanner.nextDouble();

            System.out.println("Enter the exchange rate");
            double rate = scanner.nextDouble();

            System.out.println("Enter the fee percentage");
            scanner.close();

            if (youSend < minTransfer){

                System.out.println("Sorry but the amount you would like to send is too little please " +
                        "enter a value equal to or greater than " + minTransfer);
            }

            else

            {
                double feeAmount =  (youSend / 100) * fee ;
                BigDecimal bdFeeAmount = new BigDecimal(feeAmount);
                bdFeeAmount = bdFeeAmount.setScale(2, RoundingMode.HALF_UP);

                double theyGet = rate * (youSend - feeAmount);

                System.out.println("The amount being transferred before being rounded up is €" + theyGet);

                System.out.println("The total fee being charged for this transfer is £" + bdFeeAmount);

                BigDecimal bdTheyGet = new BigDecimal(theyGet);
                bdTheyGet = bdTheyGet.setScale(2, RoundingMode.HALF_UP);

                System.out.println("After factoring the fee, the receiver will be transferred €" + bdTheyGet);

            }

        }
    }

