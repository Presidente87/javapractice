package chapter2;

import java.util.Scanner;

/**
 * Created by leonelzimi on 17/07/2019.
 */
public class GrossPayCalculator {

    public static void main(String arg []){

        //Get the number of hours worked by the Employee
        System.out.println("Enter the number of hours the employee work");

        Scanner scanner = new Scanner(System.in);

        int hours =  scanner.nextInt();

        //Get the hourly pay rate from the employee
        System.out.println("Enter your hourly rate");

        double rate = scanner.nextDouble();
        scanner.close();

        //Calculate the gross pay
        double grossPay = hours * rate;

        //Display gross pay
        System.out.println("Your gross pay is £" + grossPay);

        System.out.println("Thank you for using Zimi Banking, have a great day!");
    }
}
