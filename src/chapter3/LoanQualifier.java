package chapter3;

import java.util.Scanner;

/**
 * Created by leonelzimi on 21/07/2019.
 * NESTED IF
 * To qualify for a loan, a person must be making a minumum of £30,000
 * and have been working at their current job for at least 2 years
 */
public class LoanQualifier {

    public static void main (String args []) {

        //Initialise known values

        int minSalary = 30000;
        int minEmploymentYears = 2;

        //Get values for the unknown
        System.out.println("What is your current salary?");

        Scanner scanner = new Scanner(System.in);
        double currentSalary = scanner.nextDouble();

        System.out.println("How long have you been at your current job? Please enter value in years only");
        int currentEmploymentTime = scanner.nextInt();

        //Logic for the loan qualifier

        if (currentSalary >= minSalary) {
            if (currentEmploymentTime >= minEmploymentYears) {
                System.out.println("Congratulations, based on your current salary and employment status your qualify for a loan");
            } else {
                System.out.println("Unfortunately, based on you not being with your employer for " +  minEmploymentYears + "yrs you do not qualify for a loan on this occasion");
            }
        }
            else{

            System.out.println("Unfortunately, based on you not earning a minimum of £" +  minSalary + " you do not qualify for a loan on this occasion");
            }

            //Output the results
        }
    }

