package chapter3;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 * Created by leonelzimi on 21/07/2019.
 * IF Else IF
 */
public class PoundCounter {

    public static void main (String args [])
    {

        //known values
        double targetValue = 1;

        //values we need from the user
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the total number of pennies you would like to deposit");
        double totalPennies = scanner.nextDouble();
        System.out.println("Please enter the total number of 2ps you would like to deposit");
        double total2Ps = scanner.nextDouble();
        System.out.println("Please enter the total number of 5ps you would like to deposit");
        double total5ps = scanner.nextDouble();
        System.out.println("Please enter the total number of 10ps you would like to deposit");
        double total10ps = scanner.nextDouble();
        System.out.println("Please enter the total number of 20ps you would like to deposit");
        double total20ps = scanner.nextDouble();
        System.out.println("Please enter the total number of 50ps you would like to deposit");
        double total50ps = scanner.nextDouble();
        System.out.println("Please enter the total number of pounds you would like to deposit");
        double totalPounds = scanner.nextDouble();

        double grandPennies = 0.01 * totalPennies;
        double grand2Ps = 0.02 * total2Ps;
        double grand5ps = 0.05 * total5ps;
        double grand10ps = 0.10 * total10ps;
        double grand20ps = 0.20 * total20ps;
        double grand50ps = 0.5 * total50ps;
        double grandPounds = 1 * totalPounds;


        double totalDeposit = grandPennies + grand2Ps + grand5ps + grand10ps + grand20ps + grand50ps + grandPounds;




        if (totalDeposit == targetValue){

            System.out.println("Congratulations! you deposited exactly £1.00");
        }

        else if(totalDeposit < targetValue){

            double shortfall = targetValue - totalDeposit;
            BigDecimal bdShortFall = new BigDecimal(shortfall);
            bdShortFall = bdShortFall.setScale(2, RoundingMode.HALF_UP);


            System.out.println("Damn you didn't deposit enough! You was short by £" + bdShortFall);

        }

        else
            {
                double overload = totalDeposit - targetValue;
                BigDecimal bdOverLoad = new BigDecimal(overload);
                bdOverLoad = bdOverLoad.setScale(2, RoundingMode.HALF_UP);

                System.out.println("Damn you deposited too much! You was over by £" + bdOverLoad);

            }
    }
}
