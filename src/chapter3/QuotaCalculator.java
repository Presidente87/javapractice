package chapter3;

import java.util.Scanner;

/**
 * Created by leonelzimi on 21/07/2019.
 * IF Else
 * All sales personal are expected to make at least 10 sales a week
 * For those who do, they receive a congratulatory message
 * For those who don't, they are informed of how many sales they were short
 */
public class QuotaCalculator {

    public static void main(String args[]){

        //Initialise known values
        int salary = 1000;
        int bonus = 250;
        int bonusMinValue = 10;

        //Get values for the unknown
        System.out.println("How many sales were made this week");
        Scanner scanner = new Scanner(System.in);
        int salesNumber = scanner.nextInt();
        scanner.close();

        //Quick logic for the bonus earners
        if (salesNumber >= bonusMinValue){

            salary = salary + bonus;

            System.out.println("This week based on " + salesNumber + " sales, your total salary will be £" + salary);
            System.out.println("Congratulations for meeting the target!!");
        }

        else {

            System.out.println("Unfortunately you have not met the sales taget for this week as you was " +  (bonusMinValue - salesNumber) + " sales short.");
        }



    }

    }

