package chapter3;

import java.util.Scanner;

/**
 * Created by leonelzimi on 21/07/2019.
 * IF statement
 * All sales personal get a payment of £1000 per week
 * Sales personal who make 10 or more sales get an additional £250
 */
public class SalaryCalculator {

    public static void main(String arg []){


        //Initialise known values
        int salary = 1000;
        int bonus = 250;
        int bonusMinValue = 10;

        //Get values for the unknown
        System.out.println("How many sales were made this week");
        Scanner scanner = new Scanner(System.in);
        int salesNumber = scanner.nextInt();
        scanner.close();

        //Quick logic for the bonus earners
        if (salesNumber >= bonusMinValue){

            salary = salary + bonus;
        }



        //Output

        System.out.println("This week based on " + salesNumber + " sales, your total salary will be £" + salary);

    }
}
