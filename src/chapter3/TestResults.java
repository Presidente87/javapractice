package chapter3;

import java.util.Scanner;

/**
 * Created by leonelzimi on 21/07/2019.
 * IF ELSE IF statements
 */
public class TestResults {


    public static void main (String args []){


        //Get Test score

        System.out.println("Enter your test score");

        Scanner scanner = new Scanner(System.in);
        double score = scanner.nextDouble();

        //Determine the letter grade
        char grade;

        if (score < 60)
        {
            grade = 'E';
        }

        else if (score >= 60 && score < 70){
            grade = 'D';

        }

        else if (score >= 70 && score < 80)
        {
            grade = 'C';
        }

        else if(score >= 80 && score < 90){

            grade = 'B';
        }
        else{
            grade = 'A';
        }

        System.out.print("The grade for your test score is " + grade);
    }
}
