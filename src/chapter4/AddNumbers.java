package chapter4;

import java.util.Scanner;

/**
 * Created by leonelzimi on 22/07/2019.
 * Do while loop
 */
public class AddNumbers {

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);


        boolean again;

        do {

            System.out.println("Please enter the first number you would like to add");
            double numberA = scanner.nextDouble();
            System.out.println("Please enter the second number you would like to add");
            double numberB = scanner.nextDouble();

            double total = numberA + numberB;

            System.out.println("The total sum of the numbers you entered is " + total);

            System.out.println("Do you want to add more numbers? Type in true for No and false for Yes");

            again = scanner.nextBoolean();

        }while (again);

        scanner.close();
    }
}





