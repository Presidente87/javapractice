package chapter4;

import java.util.Scanner;

/**
 * Created by leonelzimi on 25/07/2019.
 * Nested Loop
 */
public class AverageTestScores {

    public static void main(String args []){

        int numberOfStudents = 4;
        int numberOfTests = 4;
        Scanner scanner = new Scanner(System.in);

        for(int i = 0; i< numberOfStudents; i++){

            double total = 0;

            for(int j =0; j< numberOfTests; j ++){

                System.out.println("Please enter your score for test #" + (j+1) + " for student #" + (i+1));
                double score = scanner.nextInt();

                total = total + score;

            }
            double averageScore = total/ numberOfTests;
            System.out.println("The average score for student #" +(i+1) + " is " + averageScore);
        }

        scanner.close();
    }
}
