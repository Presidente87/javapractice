package chapter4;

import java.awt.*;
import java.util.Scanner;

/**
 * Created by leonelzimi on 23/07/2019.
 * FOR Loop
 */
public class Cashier {

    public static void main (String args []){

        //get the number of items to scan
        System.out.println("Please enter the number of items you would like to scan");
        Scanner scanner = new Scanner(System.in);

        int itemCount = scanner.nextInt();


        double total = 0;

        //create a for loop

        for(int i =0; i< itemCount;i++){

            System.out.println("What is the cost of item number " + (i + 1) + "?");
            double price = scanner.nextDouble();
            total = total + price;

        }

        System.out.println("The total number of items scanned is " + itemCount + " and the total cost is £" + total);
        scanner.close();
    }
}
