package chapter4;

import java.util.Scanner;

/**
 * Created by leonelzimi on 22/07/2019.
 * While loop
 */
public class GrossPayInputValidation {

    public static void main(String args []){

        //initiliase know variables
        int rate = 20;
        int maxHours = 40;

        //get values for unknown values
        System.out.println("How many hours did you work this week?");
        Scanner scanner = new Scanner(System.in);
        double hoursWorked = scanner.nextDouble();

        while (hoursWorked < 1 || hoursWorked > maxHours)
        {
            System.out.println("Invalid number of hours entered, the minimum is 1 hour and maximum is 40 hours. Please try again");
            hoursWorked = scanner.nextDouble();
        }

        scanner.close();

        //calculate gross pas

        double grossPay = rate * hoursWorked;
        System.out.println("Your total gross pay is £" + grossPay);
    }
}
