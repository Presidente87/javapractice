package chapter4;

import java.util.Scanner;

/**
 * Created by leonelzimi on 24/07/2019.
 * Loop break
 */
public class LetterSearch {
    public static void main (String ars[]){

        //Get user to input some text
        System.out.println("Enter your favour city");
        Scanner scanner = new Scanner(System.in);
        String favCity = scanner.next();
        scanner.close();

        boolean letterFound = false;
        //search for letter a

        for (int i = 0; i < favCity.length();i++){

            char currentLetter = favCity.charAt(i);

            if (currentLetter == 'A' || currentLetter == 'a'){
                letterFound= true;
                break;

            }
        }

        if (letterFound){
            System.out.println("This city contains the letter 'A' ");
        }

        else{

            System.out.println("This city does not contain the letter 'A' ");

        }


    }
}
