package chapter5;

import java.util.Scanner;

/**
 * Created by leonelzimi on 29/07/2019.
 * Instant credit check program
 * Approves applicants who make more than £25,000 and have a credit score of 700 or greater, reject all others
 */
public class InstantCreditCheck {

    static double minSalary = 25000;
    static int minCreditScore = 700;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String args[]){

        //Initialise variable


       int creditScore = getCreditScore();
        double salary = getSalary();

        boolean isApproved = considerApplication(salary,creditScore, minSalary, minCreditScore);




        if (isApproved){

            System.out.println("Congratulations! You have been instantly approved for credit ");
        }

        else{

            System.out.println("Unfortunately we cannot offer your credit based on your current circumstances");
        }

    }

    public static boolean considerApplication(double salary, int creditScore,double minSalary, int minCreditScore  ){

        if (salary >= minSalary && creditScore >= minCreditScore){
            return true;
        }

        else{

           return false;
        }


    }

    public static int getCreditScore(){


        System.out.println("Please enter your current credit score and hit enter when done");
        int creditScore = scanner.nextInt();


        return creditScore;
    }

    public static double getSalary(){

        System.out.println("Please enter your current salary and hit enter when done");
        double salary = scanner.nextDouble();
        scanner.close();

        return salary;
    }
}
