package chapter5;

import java.util.Scanner;

/**
 * Created by leonelzimi on 29/07/2019.
 */
public class PhoneBillCalculator {

    static Scanner scanner = new Scanner(System.in);

    static double fee = 0.25;
    static double tax = 0.15;

    public static void main(String args[]){

        //method for getting plan fee
        double planCost = getPlanCost();

        //method for getting overage minutes
        int overMins = getOverageMins();

        //method for calculating overage cost
        double overageCost = overageCost(fee,overMins);


        //method for calculating the tax to be applied
        double totalTax = taxCalculator(planCost,overageCost);

        // add up all costs

        double grandTotal = totalBill(overageCost, planCost, totalTax);

        //Method to Print itemised bill

        itemisedBill(overageCost,planCost,totalTax, grandTotal);

        scanner.close();
    }


    public static double getPlanCost(){

        System.out.println("Please enter your basic plan price");
        double planCost = scanner.nextDouble();

        return planCost;
    }

    public static int getOverageMins(){

        System.out.println("Please enter the total number of minutes you have gone over by");
        int mins = scanner.nextInt();

        return mins;
    }

    public static double overageCost(double fee, int mins){

        double overageCost = fee * mins;
        return overageCost;
    }

    public static double taxCalculator(double planCost, double overageCost){

        double totalTax = (overageCost + planCost);

        totalTax = totalTax * tax;

        return totalTax;
    }

    public static double totalBill(double cost,double planCost, double totalFee){


        double grandTotal = cost + planCost + totalFee;

        return grandTotal;
    }

    public static void itemisedBill(double overageCost,double planCost, double totalTax, double grandTotal){

        System.out.println("Phone Bill Statement");
        System.out.println("Plan: £" + String.format("%.2f",planCost));
        System.out.println("Overage: £" + String.format("%.2f",overageCost));
        System.out.println("Tax: £" + String.format("%.2f",totalTax));
        System.out.println("Total: £" + String.format("%.2f",grandTotal));

    }



}
